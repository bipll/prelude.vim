command -nargs=1 -range SC <line1>,<line2>call SubsCase(<q-args>)
command -nargs=1 -range Sc <line1>,<line2>call SubsCase(<q-args>)

function CaseJoin(pat, rep, space)
	let type = 0
	if a:pat =~ '^\l[[:alnum:]]*\u[[:alnum:]]*$'
		return a:rep[0] . join(Map('toupper(_1[0]) . _1[1:]', a:rep[1:]), '')
	elseif a:pat =~ '^\u[[:alnum:]]*\l[[:alnum:]]*$'
		return join(Map('toupper(_1[0]) . _1[1:]', a:rep), '')
	elseif a:pat =~ '^\u\+_[A-Z_]*$'
		return join(Map(function('toupper'), a:rep), '_')
	elseif a:pat =~ '^\l\+_[a-z_]*$'
		return join(a:rep, '_')
	elseif a:pat =~ '^[A-Z ]*$'
		return join(Map(function('toupper'), a:rep), ' ')
	else
		return join(a:rep, a:space)
	endif
endfunction

function SubsCase(command)
	let separator = a:command[0]
	let comps = split(a:command[1:], separator)
	if len(comps) < 3
		let [pat, rep] = comps
		let flg = ''
	else
		let [pat, rep, flg] = comps
		if flg =~ 'g'
			let flg = 'g'
		else
			let flg = ''
		endif
	endif
	let pat = substitute(pat, '[ _]', '[ _]\\?', 'g')
	let pat = '\c' . substitute(pat, '\l\zs\ze\u', '[ _]\\?', 'g')
	let rep = substitute(rep, '\l\zs\ze\u', ' ', 'g')
	let space = rep =~ '_'? '_' : ' '
	let rep = substitute(rep, '_', ' ', 'g')
	let reps = split(tolower(rep))
	let i = a:firstline
	while i <= a:lastline
		let s = getline(i)
		let left = match(s, pat)
		if left >= 0
			let t = left > 0? s[0 : left - 1] : ''
			while left >= 0
				let right = matchend(s, pat, left)
				if right > left
					let mch = s[left : right - 1]
					let t .= CaseJoin(mch, reps, space)
				endif
				let left = match(s, pat, right)
				let t .= left >= 0? s[right : left - 1] : s[right:]
			endwhile
			call setline(i, t)
		endif
		let i += 1
	endwhile
endfunction
