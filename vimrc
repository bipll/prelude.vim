unlet! skip_defaults_vim
source $VIMRUNTIME/defaults.vim

packadd! matchit
filetype plugin indent on
syntax on

set mouse=
set ttymouse=

let g:guard = 1

autocmd BufWrite *.h :silent call GuardHeader()

autocmd BufRead *.kt :set tabstop=4 | set shiftwidth=4 | set expandtab

command -range LC silent call LineCont(<line1>, <line2>)
"command -nargs=1 -range SC silent call SubsCase(<line1>, <line2>, <q-args>)
command MP call SwitchPairs()
noremap M :MP<Enter>

command Unguard :let g:guard = 0
command Reguard :let g:guard = 1

:nmap <ScrollWheelUp> <nop>
:nmap <S-ScrollWheelUp> <nop>
:nmap <C-ScrollWheelUp> <nop>
:nmap <ScrollWheelDown> <nop>
:nmap <S-ScrollWheelDown> <nop>
:nmap <C-ScrollWheelDown> <nop>
:nmap <ScrollWheelLeft> <nop>
:nmap <S-ScrollWheelLeft> <nop>
:nmap <C-ScrollWheelLeft> <nop>
:nmap <ScrollWheelRight> <nop>
:nmap <S-ScrollWheelRight> <nop>
:nmap <C-ScrollWheelRight> <nop>

:imap <ScrollWheelUp> <nop>
:imap <S-ScrollWheelUp> <nop>
:imap <C-ScrollWheelUp> <nop>
:imap <ScrollWheelDown> <nop>
:imap <S-ScrollWheelDown> <nop>
:imap <C-ScrollWheelDown> <nop>
:imap <ScrollWheelLeft> <nop>
:imap <S-ScrollWheelLeft> <nop>
:imap <C-ScrollWheelLeft> <nop>
:imap <ScrollWheelRight> <nop>
:imap <S-ScrollWheelRight> <nop>
:imap <C-ScrollWheelRight> <nop>

:vmap <ScrollWheelUp> <nop>
:vmap <S-ScrollWheelUp> <nop>
:vmap <C-ScrollWheelUp> <nop>
:vmap <ScrollWheelDown> <nop>
:vmap <S-ScrollWheelDown> <nop>
:vmap <C-ScrollWheelDown> <nop>
:vmap <ScrollWheelLeft> <nop>
:vmap <S-ScrollWheelLeft> <nop>
:vmap <C-ScrollWheelLeft> <nop>
:vmap <ScrollWheelRight> <nop>
:vmap <S-ScrollWheelRight> <nop>
:vmap <C-ScrollWheelRight> <nop>

source ~/src/prelude.vim/prelude.vim
source ~/src/prelude.vim/subsCase.vim

function FindLong()
	let i = line('.')
	while i <= line('$')
		let s = getline(i)
		if strdisplaywidth(s) > 169
			call cursor(i, 1)
			break
		endif
		let i += 1
	endwhile
	if i > line('$')
		echo 'Every line looks short enough'
	endif
endfunction

function SwitchPairs()
	if &matchpairs =~ '<'
		set matchpairs-=<:>
		set rulerformat=
	else
		set matchpairs+=<:>
		set rulerformat=%20(<>\ %l,%c%V%=%P%)
	endif
endfunction

function GuardHeader()
	if !g:guard
		return
	endif
	let i1 = nextnonblank(1)
	if getline(i1) =~ '^\s*\/[/*]'
		while i1 < line('$') && getline(i1) !~ '^\s*$'
			let i1 += 1
		endwhile
	elseif i1 > 1
		execute "normal!gg" . (i1 - 1) . "dd"
		let i1 = 1
	endif
	let i2 = prevnonblank(line('$'))
	if i1 > i2
		return
	endif
	let i1 = nextnonblank(i1)

	let l1 = getline(i1)
	let guardPos = match(l1, '^#ifndef \zs')
	if guardPos > 0
		let guardName = strpart(l1, guardPos)
		let pat = '^#\s*define\s*' . guardName
		if getline(i1 + 1) !~ pat
			let guardPos = 0
		endif
	endif
	if guardPos > 0
		let guardName = strpart(l1, guardPos)
		let hashPos = match(guardName, '_[[:alnum:]]\{40\}$')
		if hashPos >= 0
			let guardName = strpart(guardName, 0, hashPos)
		endif
		if match(getline(i2), '^#\s*endif') >= 0
			let i2 = prevnonblank(i2 - 1)
		endif
	else
		let guardName = substitute(bufname(''), 'include/', '', '')
		if match(guardName, '/') < 0
			let dirPath = substitute(getcwd(), '/include', '', '')
			let guardName = substitute(dirPath, '.*/', '', '') . '/' . guardName
		else
			let guardName = substitute(guardName, '.*/\ze.*/', '', '')
		endif
		let guardName = toupper(substitute(substitute(substitute(
					\ guardName, '\l\zs\ze\u\|\.', '_', 'g'), '/', '__', 'g'), '[^[:alnum:]_]', '_', 'g'))
		execute "normal!" . i1 . "GO\n\n"
		let i2 += 3
	endif
	if i1 <= i2
		let guardName .= '_' . system('sha1sum', join(getline(i1 + 3, i2), '\n') . '\n')[0:39]
		call setline(i1, '#ifndef ' . guardName)
		call setline(i1 + 1, '#define ' . guardName)
		call setline(i2 + 1, '')
		call setline(i2 + 2, '#endif')
		if i2 < line('$') - 2
			execute 'normal ' . (i2 + 3) . 'GdG'
		endif
	endif
endfunction

function LineCont(left, right)
	let left = a:left
	let right = a:right
	let s = getline(left)
	while left > 1 && s[-1:] == '\'
		let left -= 1
		let s = getline(left)
	endwhile
	if left < a:left && s[-1:] != '\'
		let left += 1
	endif
	let s = getline(right)
	while right < line('$') && s[-1:] == '\'
		let right += 1
		let s = getline(right)
	endwhile
	let m = 0
	let i = left
	while i <= right
		let n = strdisplaywidth(substitute(getline(i), '\s*\\\?$', '', ''))
		if n < 160 && n > m
			let m = n
		endif
		let i += 1
	endwhile
	let m = m / 8 + 1
	let i = left
	while i < right
		let s = substitute(getline(i), '\s*\\\?$', '', '')
		let n = strdisplaywidth(s) / 8
		if n < m
			let s .= repeat("\t", m - n)
		endif
		call setline(i, s . '\')
		let i += 1
	endwhile
endfunction

function VimMap(list, expr)
	return map(copy(a:list), a:expr)
endfunction

function StdHeader(...)
	let stdHeader = bufname('')[0:-3]
	call cursor(1, 1)
	call search('^#ifndef')
	execute "normal d/^#\\s*include\\s*<" . stdHeader . ">/-1\<Enter>"
	let funArgs = copy(a:000)
	if a:0 > 0 && funArgs[0] == '0'
		call remove(funArgs, 0)
	else
		let stdHeader = '\<' . stdHeader . '\>'
	endif
	if len(funArgs) > 0
		if funArgs[0] == 20
			execute "normal :s/^#\\s*\\(include\\s*\\(<.*>\\)\\)/#if __has_include(\\2)\\r\\r#\\1\<Enter>"
			if len(funArgs) == 1
				let pat = stdHeader
			elseif funArgs[1] == -1
				let pat = join(funArgs[2:-1], '\|')
			else
				let pat = join([stdHeader] + funArgs[1:-1], '\|')
			endif
		elseif funArgs[0] == -1
			let pat = join(funArgs[1:-1], '\|')
		else
			let pat = join([stdHeader] + funArgs, '\|')
		endif
	else
		let pat = stdHeader
	endif
	execute "normal jd/" . pat . "/-1\<Enter>O\<Esc>/^\s*$\<Enter>dG"
endfunction

function TagsPath()
	let current = expand('%:p:h')
	let x = 0
	while current != ''
		if filereadable(current . '/tags')
			return current . '/tags'
		endif
		let idx = strridx(current, '/') - 1
		if idx < 0
			return ''
		endif
		let current = current[:idx]
		let x += 1
		if x == 5
			return ''
		endif
	endwhile
	return ''
endfunction

"execute 'set tags=' . TagsPath()
set tags=./tags;,tags;


function Omg(templateArgs)
	let items = split(a:templateArgs, ', ')
	let i = 0
	while i < len(items)
		let words = split(items[i])
		let amount = len(words)
		if words[amount - 2] == '...' || words[amount - 2] == 'class...'
			let item = words[amount - 1] . '...'
		else
			let item = words[amount - 1]
		endif
		let items[i] = item
		let i += 1
	endwhile
	return join(items, ', ')
endfunction

function UVT()
	let @u = ":s/template\\s*<\\(.*\\)> \\(struct\\|class\\|using\\) \\(\\i*\\).*/\\='template<' . submatch(1) . '> using ' . submatch(3) . 'T = typename ' . submatch(3) . '<' . Omg(submatch(1)) . '>::type;'"
	let @v = ":s/template\\s*<\\(.*\\)> \\(struct\\|class\\|using\\) \\(\\i*\\).*/\\='template<' . submatch(1) . '> static constexpr auto ' . submatch(3) . 'V = ' . submatch(3) . '<' . Omg(submath(1)) . '>::value;'"
endfunction

function UV_t()
	let @u = ":s/template\\s*<\\(.*\\)> \\(struct\\|class\\|using\\) \\(\\i*\\).*/\\='template<' . submatch(1) . '> using ' . submatch(3) . '_t = typename ' . submatch(3) . '<' . Omg(submatch(1)) . '>::type;'"
	let @v = ":s/template\\s*<\\(.*\\)> \\(struct\\|class\\|using\\) \\(\\i*\\).*/\\='template<' . submatch(1) . '> static constexpr auto ' . submatch(3) . '_v = ' . submatch(3) . '<' . Omg(submath(1)) . '>::value;'"
endfunction

call UVT()
