let g:TList = type([])
let g:TFunc = type(function('tr'))
let g:TStr = type('')



function ApplyRef(funcRef, args)
	return call(a:funcRef, a:args)
endfunction

function ApplyStr(funcStr, args)
	let funcStr = a:funcStr
	let i = 1
	for a in a:args
		let funcStr = substitute(funcStr, '\<_' . i . '\>', string(a), 'g')
		let i += 1
	endfor
	return eval(funcStr)
endfunction

function Apply(func, args)
	let funcType = type(a:func)
	if funcType == g:TList
		let retVal = copy(a:func)
		return map(retVal, 'Apply(v:val, ' . string(args) . ')')
	elseif funcType == g:TFunc
		return ApplyRef(a:func, a:args)
	elseif funcType == g:TStr
		return ApplyStr(a:func, a:args)
	else
		return ApplyStr(string(a:func), a:args)
	endif
endfunction



function EvalRef(funcRef, ...)
	return ApplyRef(a:funcRef, a:000)
endfunction

function EvalStr(funcStr, ...)
	return ApplyStr(a:funcStr, a:000)
endfunction

function Eval(func, ...)
	return Apply(a:func, a:000)
endfunction



function Row(i, matrix)
	let retVal = range(len(a:matrix))
	for j in retVal
		let retVal[j] = a:matrix[j][a:i]
	endfor
	return retVal
endfunction


function MapRef(funcRef, depth, matrix)
	let retVal = range(a:depth)
	for i in retVal
		let retVal[i] = ApplyRef(a:funcRef, Row(i, a:matrix))
	endfor
	return retVal
endfunction

function MapRef1(funcRef, seq)
	let retVal = range(len(a:seq))
	for i in retVal
		let retVal[i] = ApplyRef(a:funcRef, [a:seq[i]])
	endfor
	return retVal
endfunction

function MapStr(funcStr, depth, matrix)
	let retVal = range(a:depth)
	for i in retVal
		let retVal[i] = ApplyStr(a:funcStr, Row(i, a:matrix))
	endfor
	return retVal
endfunction

function MapStr1(funcStr, seq)
	let retVal = range(len(a:seq))
	for i in retVal
		let retVal[i] = ApplyStr(a:funcStr, [a:seq[i]])
	endfor
	return retVal
endfunction

function Map(func, ...)
	let funcType = type(a:func)
	if funcType == g:TList
		let retVal = range(len(a:func))
		for i in retVal
			let form = [func[i]]
			retVal[i] = ApplyRef(function('Map'), extend(form, a:000))
		endfor
		return retVal
	endif
	if a:0 == 1
		if funcType == g:TFunc
			return MapRef1(a:func, a:1)
		else
			return MapStr1(a:func, a:1)
		endif
	endif
	let [depth, i] = [len(a:1), 2]
	while i <= a:0
		let di = len(a:{i})
		if di < depth
			let depth = di
		endif
		let i += 1
	endwhile
	if depth == 0
		return []
	endif
	let form = extend([depth, a:func], a:000)
	if funcType == g:TFunc
		return MapRef(a:func, depth, a:000)
	else
		return MapStr(a:func, depth, a:000)
	endif
endfunction



function FilterRef(funcRef, seq)
	let retVal = []
	for x in a:seq
		if EvalRef(a:funcRef, x)
			call append(retVal, x)
		endif
	endfor
	return retVal
endfunction

function FilterStr(funcStr, seq)
	let retVal = []
	for x in a:seq
		if EvalStr(a:funcStr, x)
			call append(retVal, x)
		endif
	endfor
	return retVal
endfunction

function Filter(func, seq)
	let funcType = type(a:func)
	if funcType == g:TFunc
		return FilterRef(a:func, a:seq)
	else
		return FilterStr(a:func, a:seq)
	endif
endfunction



function Cons(...)
	return extend(a:000[: a:0 - 2], a:{a:0})
endfunction



function Splice(...)
	if a:0 == 0
		return []
	endif
	if type(a:1) == g:TList
		let retVal = a:1
	else
		let retVal = [a:1]
	endif
	let i = 2
	while i <= a:0
		if type(a:{i}) == g:TList
			call extend(retVal, a:{i})
		else
			call append(retVal, a:{i})
		endif
		let i += 1
	endwhile
	return retVal
endfunction



function FlatMapRef(funcRef, depth, matrix)
	let retVal = []
	let i = 0
	while i < a:depth
		for j in ApplyRef(a:funcRef, Row(i, a:matrix))
			call add(retVal, j)
		endfor
		let i += 1
	endwhile
	return retVal
endfunction

function FlatMapRef1(funcRef, seq)
	let retVal = []
	let i = 0
	while i < len(a:seq)
		for j in ApplyRef(a:funcRef, [a:seq[i]])
			call add(retVal, j)
		endfor
		let i += 1
	endwhile
	return retVal
endfunction

function FlatMapStr(funcStr, depth, matrix)
	let retVal = []
	let i = 0
	while i < a:depth
		for j in ApplyStr(a:funcStr, Row(i, a:matrix))
			call add(retVal, j)
		endfor
		let i += 1
	endwhile
	return retVal
endfunction

function FlatMapStr1(funcStr, seq)
	let retVal = []
	let i = 0
	while i < len(a:seq)
		for j in ApplyStr(a:funcStr, [a:seq[i]])
			call add(retVal, j)
		endfor
		let i += 1
	endwhile
	return retVal
endfunction

function FlatMap(func, ...)
	let funcType = type(a:func)
	if funcType == g:TList
		let retVal = range(len(a:func))
		for i in retVal
			let form = [func[i]]
			let retVal[i] = ApplyRef(function('FlatMap'), extend(form, a:000))
		endfor
		return retVal
	endif
	if a:0 == 1
		if funcType == g:TFunc
			return FlatMapRef1(a:func, a:1)
		else
			return FlatMapStr1(a:func, a:1)
		endif
	endif
	let [depth, i] = [len(a:1), 2]
	while i <= a:0
		let di = len(a:{i})
		if di < depth
			let depth = di
		endif
		let i += 1
	endwhile
	if depth == 0
		return []
	endif
	let form = extend([depth, a:func], a:000)
	if funcType == g:TFunc
		return FlatMapRef(a:func, depth, a:000)
	else
		return FlatMapStr(a:func, depth, a:000)
	endif
endfunction


function Sub(formula)
	let parts = split(a:formula, '\\\\')
	let i = 0
	while i < len(parts)
		let [firstChar, part] = parts[i][0] == '&'? ['_1', parts[i][1:]] : ['', parts[i]]
		let part = substitute(part, '[^\\]\zs&', "_1", 'g')
		let j = 0
		while j < 10
			let part = substitute(part, '\\' . j, "_" . (j + 1), 'g')
			let j += 1
		endwhile
		let parts[i] = firstChar . part
		let i += 1
	endwhile
	let f = join(parts, '\\')
	return EvalStr(f, submatch(0), submatch(1), submatch(2), submatch(3), submatch(4), submatch(5), submatch(6), submatch(7), submatch(8), submatch(9))
endfunction

command -nargs=1 -range Cl call Columns(<f-args>, <line1>, <line2>)
function Columns(pattern, from, upto)
	let pattern = '[' . a:pattern . ']\zs'
	let words = []
	let i = a:from
	while i <= a:upto
		let currentLine = split(getline(i), pattern, 1)
		let j = 0
		while j < len(words) && j < len(currentLine)
			let wordLen = strlen(currentLine[j])
			if wordLen > words[j]
				let words[j] = wordLen
			endif
			let j += 1
		endwhile
		while j < len(currentLine)
			call add(words, strlen(currentLine[j]))
			let j += 1
		endwhile
		let i += 1
	endwhile
	let i = a:from
	while i <= a:upto
		let currentLine = split(getline(i), pattern, 1)
		let rv = ''
		let j = 0
		while j + 1 < len(currentLine)
			let currentWord = currentLine[j]
			let rv .= currentWord[:-2] . repeat(' ', words[j] - strlen(currentWord)) . currentWord[-1:]
			let j += 1
		endwhile
		call setline(i, rv . currentLine[-1])
		let i += 1
	endwhile
endfunction
